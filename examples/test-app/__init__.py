import os
import sys

from flask import Flask

sys.path.append('../..')

from spook.spook import Spook

if __name__ == '__main__':
    app = Flask(__name__)
    app.config['DEBUG'] = True

    spook = Spook(app)
    spook.template = os.path.join(os.getcwd(), './template.html')

    spook.render('/', "Dashboard")

    spook.run()
