from spook.controller import Controller
from spook.event import Event
from spook.input import Input
from spook.output import Output


class TopLeads(Controller):
    data = list([{
        "id": "javascript",
        "label": "javascript",
        "value": 555,
        "color": "hsl(55, 70%, 50%)"
    },
        {
            "id": "erlang",
            "label": "erlang",
            "value": 458,
            "color": "hsl(334, 70%, 50%)"
        },
        {
            "id": "python",
            "label": "python",
            "value": 224,
            "color": "hsl(229, 70%, 50%)"
        },
        {
            "id": "go",
            "label": "go",
            "value": 471,
            "color": "hsl(30, 70%, 50%)"
        },
        {
            "id": "haskell",
            "label": "haskell",
            "value": 155,
            "color": "hsl(280, 70%, 50%)"
        },
        {
            "id": "php",
            "label": "php",
            "value": 119,
            "color": "hsl(174, 70%, 50%)"
        },
        {
            "id": "stylus",
            "label": "stylus",
            "value": 591,
            "color": "hsl(303, 70%, 50%)"
        },
        {
            "id": "lisp",
            "label": "lisp",
            "value": 44,
            "color": "hsl(72, 70%, 50%)"
        },
        {
            "id": "ruby",
            "label": "ruby",
            "value": 338,
            "color": "hsl(167, 70%, 50%)"
        },
        {
            "id": "java",
            "label": "java",
            "value": 256,
            "color": "hsl(290, 70%, 50%)"
        },
        {
            "id": "c",
            "label": "c",
            "value": 452,
            "color": "hsl(93, 70%, 50%)"
        },
        {
            "id": "hack",
            "label": "hack",
            "value": 110,
            "color": "hsl(273, 70%, 50%)"
        },
        {
            "id": "scala",
            "label": "scala",
            "value": 241,
            "color": "hsl(206, 70%, 50%)"
        },
        {
            "id": "rust",
            "label": "rust",
            "value": 429,
            "color": "hsl(248, 70%, 50%)"
        },
        {
            "id": "sass",
            "label": "sass",
            "value": 3,
            "color": "hsl(169, 70%, 50%)"
        },
        {
            "id": "elixir",
            "label": "elixir",
            "value": 310,
            "color": "hsl(242, 70%, 50%)"
        },
        {
            "id": "make",
            "label": "make",
            "value": 384,
            "color": "hsl(297, 70%, 50%)"
        },
        {
            "id": "css",
            "label": "css",
            "value": 222,
            "color": "hsl(198, 70%, 50%)"
        }])

    def define_callbacks(self):
        @self.callback(Output('#categories', 'options'),
                       events=[Event('#test-input', 'onChange')])
        def categories():
            return map(lambda data: {
                'label': data['id'], 'value': data['id']
            }, TopLeads.data)

        @self.callback(Output('#pie', 'data'),
                       [Input('#categories', 'value')],
                       events=[Event('#test-input', 'onChange'), Event('#categories', 'onChange')])
        def pie_data(value):
            def process_data(data):
                if data['id'] == value['value']:
                    data['color'] = '#fff'
                print(value, data)
                return data

            return map(process_data, TopLeads.data)
