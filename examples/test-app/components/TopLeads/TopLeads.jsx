import * as React from "react"
import "./TopLeads.scss"
import Select from "react-select"
import {ResponsivePieCanvas} from "@nivo/pie";

export default (controller) => <div>
            <input id={"test-input"}/>
            <button className={"btn btn-primary"} id="test-button">Do a thing</button>
            <Select id={"categories"} onChange={(category) => controller.setState({category})} value={controller.state.category}
                    options={[
                        {value: 'hello', label: 'hello'}, {value: 'smello', label: 'smello'}]}/>
            <ResponsivePieCanvas
                data={[]}
                id={"pie"}
                margin={{
                    "top": 40,
                    "right": 200,
                    "bottom": 40,
                    "left": 80
                }}
                pixelRatio={1}
                innerRadius={0.5}
                padAngle={0.7}
                cornerRadius={3}
                colors="d320b"
                colorBy="id"
                borderColor="inherit:darker(0.6)"
                radialLabelsSkipAngle={10}
                radialLabelsTextXOffset={6}
                radialLabelsTextColor="#333333"
                radialLabelsLinkOffset={0}
                radialLabelsLinkDiagonalLength={16}
                radialLabelsLinkHorizontalLength={24}
                radialLabelsLinkStrokeWidth={1}
                radialLabelsLinkColor="inherit"
                slicesLabelsSkipAngle={10}
                slicesLabelsTextColor="#333333"
                animate={true}
                motionStiffness={90}
                motionDamping={15}
                theme={{
                    "tooltip": {
                        "container": {
                            "fontSize": "13px"
                        }
                    },
                    "labels": {
                        "textColor": "#555"
                    }
                }}
                defs={[
                    {
                        "id": "dots",
                        "type": "patternDots",
                        "background": "inherit",
                        "color": "rgba(255, 255, 255, 0.3)",
                        "size": 4,
                        "padding": 1,
                        "stagger": true
                    },
                    {
                        "id": "lines",
                        "type": "patternLines",
                        "background": "inherit",
                        "color": "rgba(255, 255, 255, 0.3)",
                        "rotation": -45,
                        "lineWidth": 6,
                        "spacing": 10
                    }
                ]}
                fill={[
                    {
                        "match": {
                            "id": "ruby"
                        },
                        "id": "dots"
                    },
                    {
                        "match": {
                            "id": "c"
                        },
                        "id": "dots"
                    },
                    {
                        "match": {
                            "id": "go"
                        },
                        "id": "dots"
                    },
                    {
                        "match": {
                            "id": "python"
                        },
                        "id": "dots"
                    },
                    {
                        "match": {
                            "id": "scala"
                        },
                        "id": "lines"
                    },
                    {
                        "match": {
                            "id": "lisp"
                        },
                        "id": "lines"
                    },
                    {
                        "match": {
                            "id": "elixir"
                        },
                        "id": "lines"
                    },
                    {
                        "match": {
                            "id": "javascript"
                        },
                        "id": "lines"
                    }
                ]}
                legends={[
                    {
                        "anchor": "right",
                        "direction": "column",
                        "translateX": 140,
                        "itemWidth": 60,
                        "itemHeight": 14,
                        "itemsSpacing": 2,
                        "symbolSize": 14,
                        "symbolShape": "circle"
                    }
                ]}
            />
        </div>
