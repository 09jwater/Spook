import os

from flask.json import JSONEncoder


def generate_js(spook, route):
    data = {}
    imports = []

    for name in spook.controllers:
        controller = spook.controllers[name]

        data[name] = {
            'name': name,
            'callbacks': list(map(lambda callback: callback.serialize(), controller.callbacks))
        }

        controller_file_name = spook.get_controller_file_name(name)

        if os.path.exists(controller_file_name + '.jsx'):
            controller_file_name = os.path.relpath(controller_file_name, os.getcwd()).replace(os.sep, '/')
            imports.append(
                'window.' + name + ' = spook.wrapControllerComponent(require("../' + controller_file_name + '").default);')

    encoder = JSONEncoder()

    lines = [
        "import Spook from '../../../client/spook';",
        "import * as React from 'react';",
        "import * as ReactDOM from 'react-dom';",
        "",
        "let spook = window.__spook = new Spook({",
        "   host: 'localhost:5000',"
        "   controllers: " + encoder.encode(data) + ",",
        "});",
        "window.onload = () => spook.render(window." + route.base_controller + ");"
    ]
    lines.extend(imports)

    for stylesheet in route.stylesheets:
        path = os.path.abspath(stylesheet)
        path = path.replace(os.sep, '/')
        lines.append("import '" + path + "'")

    with open(route.src_path, "w") as file:
        file.writelines(list(map(lambda line: line + '\n', lines)))
