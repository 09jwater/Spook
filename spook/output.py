import hashlib


class Output:

    def __init__(self, selector, value):
        self.selector = selector
        self.value = value

    def serialize(self):
        key = hashlib.md5(str(self.selector + '-' + self.value).encode()).hexdigest()

        return {
            'selector': self.selector,
            'value': self.value,
            'key': key
        }
