import hashlib


class Event:

    def __init__(self, selector, callback):
        self.selector = selector
        self.callback = callback

    def serialize(self):
        key = hashlib.md5(str(self.selector + '-' + self.callback).encode()).hexdigest()

        return {
            'selector': self.selector,
            'callback': self.callback,
            'key': key
        }