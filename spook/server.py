import logging

from flask import send_from_directory, request, jsonify

from spook.json_encoder import BaseEncoder

# Shutup flask
log = logging.getLogger('werkzeug').setLevel(logging.ERROR)


class Server:

    def __init__(self, spook, app):
        app.json_encoder = BaseEncoder

        @app.route('/.spook/resources/<path:path>')
        def resources(path):
            return send_from_directory(spook.get_build_dir(), path)

        @app.route('/.spook/callback', methods=["POST"])
        def callback():
            content = request.get_json(silent=True)
            return jsonify(spook.process_callback(content))
