import abc

from spook.callback import Callback


class Controller(abc.ABC):

    def __init__(self):
        self.callbacks = []

    @abc.abstractmethod
    def define_callbacks(self):
        pass

    def callback(self, output, inputs=list(), events=list()):
        def callback_decorator(original_function):
            self.callbacks.append(Callback(original_function, output, inputs, events))

        return callback_decorator
