from json import JSONEncoder


class BaseEncoder(JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'serialize'):
            return obj.serialize()
        else:
            try:
                iterable = iter(obj)
            except TypeError:
                pass
            else:
                return list(iterable)

        return JSONEncoder.default(self, obj)