class Input:

    def __init__(self, selector, value):
        self.selector = selector
        self.value = value

    def serialize(self):
        return {
            'selector': self.selector,
            'value': self.value
        }