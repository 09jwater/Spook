import hashlib
import os

from pyquery import PyQuery

from spook import webpack


class Route:

    def __init__(self, spook, url, base_controller):
        self.id = hashlib.md5(str('spook-route:' + url).encode()).hexdigest()[::3]
        self.url = url
        self.base_controller = base_controller

        src_path = os.path.join(spook.get_build_dir(), self.id + '.generated.js')
        base_name = self.id + '.build.min.js'
        dist_path = os.path.join(spook.get_build_dir(), base_name)

        with open(spook.template, "r") as file:
            doc = PyQuery(file.read())
            doc("head").append(PyQuery('<script></script>').attr('src', '/.spook/resources/' + os.path.basename(dist_path)))
            self.stylesheets = self.extract_stylesheets(doc)
            html = doc.outer_html()

        self.html = html
        self.src_path = src_path
        self.dist_path = dist_path
        self.thread = None

    def extract_stylesheets(self, doc):
        stylesheets = []
        for el in doc("link[rel='stylesheet']").items():
            href = el.attr('href')
            if not href.startswith("http"):
                el.remove()
                stylesheets.append(href)
        return stylesheets

    def build(self, watch=True):
        self.thread = webpack.build(self, watch)
