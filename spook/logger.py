import inspect
import logging
import os

log_dir = os.path.join(os.getcwd(), 'logs')
if not os.path.exists(log_dir):
    os.makedirs(log_dir)

log_file = os.path.join(log_dir, 'spook.log')

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-30s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename=log_file,
                    filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)

logger = logging.getLogger('Spook')


class Logger:

    @staticmethod
    def _get_invoking_class():
        stack = inspect.stack()
        line = stack[3][0]

        if hasattr(line.f_locals, "self"):
            name = line.f_locals["self"].__class__.__name__
        else:
            name =line.f_globals['__name__']

        parts = name.split(".")
        return parts[len(parts) - 1].title()

    @staticmethod
    def _log(level, message):
        return logger.log(level, '[%s] %s', Logger._get_invoking_class(), message)

    @staticmethod
    def info(message):
        return Logger._log(logging.INFO, message)

    @staticmethod
    def debug(message):
        return Logger._log(logging.DEBUG, message)

    @staticmethod
    def warning(message):
        return Logger._log(logging.WARNING, message)

    @staticmethod
    def error(message):
        return Logger._log(logging.ERROR, message)

    @staticmethod
    def critical(message):
        return Logger._log(logging.CRITICAL, message)
