import atexit
import importlib.util
import inspect
import os
import signal
import sys

from spook import generator, webpack
from spook.logger import Logger
from spook.route import Route
from spook.server import Server

build_dir = os.path.join(os.getcwd(), '.spook/')
shutting_down = False
started = False

class Spook:

    def __init__(self, app):
        """

        :param app: Flask
        """
        os.makedirs(build_dir, exist_ok=True)

        self.server = Server(self, app)
        self.app = app
        self.controllers = {}
        self.routes = {}
        self.template = os.path.join(os.getcwd(), './template.html')

    @staticmethod
    def get_build_dir():
        return build_dir

    def render(self, url, base_controller):
        """
        :param url:
        :return:
        """

        route = Route(self, url, base_controller)

        self.routes[route.id] = route

        @self.app.route(url)
        def __route():
            if not self.is_template_valid():
                "Invalid template"

            return route.html

    def __build__routes(self):
        webpack.install()

        for id in self.routes:
            route = self.routes[id]
            generator.generate_js(self, route)
            route.build()

    def run(self):
        global started

        if started:
            return

        started = True

        self.__register_exit_handler()
        self.__register_controllers()
        self.__build__routes()

        Logger.info("Starting server")

        self.app.run()

    def __register_exit_handler(self):

        def exit_handler():
            global shutting_down

            if shutting_down:
                return

            shutting_down = True
            Logger.info("Shutting down server")

            webpack.kill_all()
            sys.exit()

        signal.signal(signal.SIGINT, lambda sig, frame: exit_handler())
        atexit.register(exit_handler)

    def is_template_valid(self):
        return os.path.exists(self.template)

    def __register_controllers(self, ):
        components_dir = os.path.join(os.getcwd(), './components/')
        for name in os.listdir(components_dir):
            component_path = os.path.join(components_dir, name)
            if os.path.isdir(component_path):
                controller = self.__create_controller(name)
                self.controllers[name] = controller

    def get_controller_file_name(self, name, ext=""):
        components_dir = os.path.join(os.getcwd(), './components/')
        return os.path.join(components_dir, name + "/" + name + ext)

    def get_controller(self, name):
        return self.controllers[name]

    @staticmethod
    def load_controller(name):
        path = os.path.join(os.getcwd(), "./components/" + name + "/" + name + ".py")

        if not os.path.exists(path):
            print("Error invalid component")

        spec = importlib.util.spec_from_file_location(name, path)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)

        if not hasattr(module, name):
            print("Component module must contain class with the same name, so '" + name + "'")

        return getattr(module, name)

    def __create_controller(self, name):
        controller_class = self.load_controller(name)
        controller = controller_class()
        controller.define_callbacks()
        return controller

    def process_callback(self, content):
        controller_name = content['controller']
        event = content['event']
        state = content['state']

        controller = self.controllers[controller_name]

        updates = []

        for callback in controller.callbacks:
            has_event = False

            for callback_events in callback.events:
                if callback_events.selector == event['selector'] and callback_events.callback == event['callback']:
                    has_event = True

            if has_event:

                args = {}

                for arg in inspect.getfullargspec(callback.function).args:
                    if arg in list(state.keys()):
                        args[arg] = state[arg]

                updates.append({
                    'output': callback.output,
                    'result': callback.function(**args)
                })

        return updates
