import os
import shutil
import subprocess
import sys

from spook import flags
from spook.logger import Logger

pool = None
processes = []


def get_command_path():
    return os.path.join(os.getcwd(), "./node_modules/.bin/webpack")


def is_installed():
    return os.path.exists(get_command_path())


def install(force=False):
    if not force and is_installed() and not '--force-install' in sys.argv:
        return

    args = ["npm", "install", "--save-dev"]
    args.extend([
        "webpack",
        "webpack-command",
        "babel-polyfill",
        "babel-loader@8.0.0-beta.3",
        "css-loader",
        "node-sass",
        "sass-loader",
        "style-loader",
        "static-site-generator-webpack-plugin",
        "@babel/core",
        "@babel/preset-react",
        "@babel/preset-env",
    ])

    os.system(" ".join(args))


def copy_config(id, dist_dir):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    src = os.path.join(dir_path, "static/webpack.config.js")
    dist = os.path.join(dist_dir, id + ".webpack.config.js")

    shutil.copyfile(src, dist)

    return dist


def kill_all():
    for process in processes:
        process.kill()
        process.wait()
    processes.clear()


def build(route, watch=False):
    if watch:
        Logger.info("Starting watch build for route '" + route.url + "'")

    config = copy_config(route.id, os.path.dirname(route.dist_path))

    watchArg = ""
    if watch:
        watchArg = "-w"

    babel_polyfill = os.path.join(os.getcwd(), "./node_modules/babel-polyfill/dist/polyfill.js").replace(os.sep, '/')
    if flags.is_production():
        mode = 'production'
    else:
        mode = 'development'

    process = subprocess.Popen([get_command_path(), route.src_path,
                                babel_polyfill,
                                "-o", route.dist_path,
                                "--mode", mode,
                                "--config", config,
                                "--cache",
                                "--hot",
                                watchArg
                                ], shell=True)

    processes.append(process)
