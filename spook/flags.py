import sys


def is_production():
    return '-p' in sys.argv and '--production' in sys.argv