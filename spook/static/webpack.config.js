const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');

module.exports = {
    output: {
        globalObject: "this"
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            "presets": [
                                "@babel/preset-env",
                                "@babel/preset-react",
                            ]
                        }
                    }
                ],

            },
            {
                test: /\.s?css$/,
                use: [
                    {
                        loader: "style-loader" // creates style nodes from JS strings
                    },
                    {
                        loader: "css-loader" // translates CSS into CommonJS
                    },
                    {
                        loader: "sass-loader" // compiles Sass to CSS
                    }
                ]
            }
        ]
    },
    plugins: [
        new StaticSiteGeneratorPlugin({
            paths: [
                '/',
            ]
        }),
    ],
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
};