class Callback:

    def __init__(self, function, output, inputs, events):
        self.output = output
        self.inputs = inputs
        self.events = events
        self.function = function

    def serialize(self):
        return {
            'output': self.output.serialize(),
            'inputs': list(map(lambda input: input.serialize(), self.inputs)),
            'events': list(map(lambda event: event.serialize(), self.events))
        }
