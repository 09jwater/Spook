import * as React from "react";

module.exports = {

    Spook: require('./spook'),
    view: (name) => React.createElement(window[name])

};