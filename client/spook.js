import * as React from "react";
import * as ReactDOM from "react-dom";

export default class Spook {

    constructor(data) {
        this.outputs = {};
        this.events = {};
        for (let key in data) {
            this[key] = data[key]
        }
    }

    getHost() {
        return "http://" + this.host;
    }

    render(component) {
        if (!component) {
            component = this.component;
        } else {
            this.component = component;
        }
        let root = this.root = React.createElement(component);
        let node = document.querySelector('#__main__');
        return ReactDOM[node.children.length > 0 ? 'render' : 'render'](root, node);
    }

    collectState(callback) {
        let state = {};

        for (let input of callback.inputs) {
            let element = document.querySelector(input.selector);

            const key = input.value;
            let value = element[key];
            if (!value) {
                let node = this.findReactComponent(element);
                value = node.props[key] || node.state[key];
            }

            state[key] = value;
        }

        return state;
    }

    findReactComponent(el) {
        for (const key in el) {
            if (key.startsWith('__reactInternalInstance$')) {
                const fiberNode = el[key];
                let props = {
                    ...fiberNode.return.memoizedProps,
                    ...fiberNode.return.return.memoizedProps
                };
                return {
                    props,
                    state: fiberNode.return.return.memoizedState

                };
            }
        }
        return null;
    }

    sendCallback(controller, event) {
        let state = {};

        for (let callback of this.events[event.key]) {
            state = Object.assign(state, this.collectState(callback))
        }

        fetch(new Request(this.getHost() + "/.spook/callback"), {
            method: "POST",
            body: JSON.stringify({
                controller: controller.name,
                event,
                key: event.key,
                state
            }),
            headers: {
                "Content-Type": "application/json"
            }
        }).then(response => {
            return response.json();
        }).then(content => {
            for (let update of content) {
                this.outputs[update.output.key] = update.result;
            }
            this.render()
        })
    }

    selectorMatches(id, className, selector) {
        let selectors = selector.split(" ");

        for (let part of selectors) {
            if (part.startsWith("#")) {
                return part.substr(1) == id
            }
        }

        return false
    }

    wrapChild(component) {
        if (typeof component === "string" || typeof component === "function") {
            return component
        }

        let props = {...component.props};
        let {children, id, className} = props;

        for (let controllerName in this.controllers) {
            let controller = this.controllers[controllerName]

            for (let callback of controller.callbacks) {
                for (let event of callback.events) {
                    let set = this.events[event.key]
                    if (!set) {
                        set = this.events[event.key] = new Set();
                    }

                    set.add(callback);

                    if (this.selectorMatches(id, className, event.selector)) {
                        let original = props[event.callback];
                        props[event.callback] = (...args) => {
                            this.sendCallback(controller, event, args);
                            if (original) {
                                return original.apply(args);
                            }
                        }
                    }
                }

                if (this.selectorMatches(id, className, callback.output.selector)) {
                    let value = this.outputs[callback.output.key]
                    if (value) {
                        props[callback.output.value] = value
                    }
                }
            }
        }

        if (children) {
            props.children = React.Children.map(children, child => {
                return this.wrapChild(child);
            })
        }
        return React.cloneElement(component, props)
    }

    wrapControllerComponent(component) {
        const self = this;

        return class extends React.Component {

            constructor(props) {
                super(props);

                this.state = {}
            }

            render() {
                return React.Children.map(component(this), child => {
                    return self.wrapChild(child);
                })
            }
        }
    }

}